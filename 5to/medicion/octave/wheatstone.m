printf("Matriz de datos de 3x3, leida del archivo \'matriz\' :\n");

mA = dlmread("matriz", '&', 0,0)

printf("Ingrese la matriz B\n");

mB = [input("d1:");
      input("d2:");
      input("d3:");]

mainDet = det(mA);

if mainDet == 0
  disp("El primer determinante es invalido, cancelando operacion...\n")
  quit()
endif

for i = 1:3
  dets{i} = mA
  dets{i}(:,i) = mB
  dets{i} = det(dets{i})
endfor

printf("I_2 = %.3e \n", dets{1}/mainDet)
printf("I_4 = %.3e \n", dets{2}/mainDet)
printf("I_g = %.3e \n", dets{3}/mainDet)
